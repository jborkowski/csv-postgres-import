package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	"database/sql"

	"github.com/gocarina/gocsv"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

const (
	CORE_N = 2
	HDD_N  = 1

	N_CONNS     = ((CORE_N * 2) + HDD_N)
	DB_USER     = "dbuser"
	DB_PASSWORD = "Test@123??"
	DB_NAME     = "bss"
	DB_HOST     = "192.168.20.214"

	QUERY_RESOURCE_ID  = `SELECT employee_id as id FROM myprofile.vw_resources a where a.employee_full_name ilike reorder_name($1)` //AND a.department_code ilike '$2'
	QUERY_PROJECT_CODE = `SELECT proj_id as id FROM resourcetool.vw_projects p WHERE p.proj_code ILIKE $1 LIMIT 1`
)

var (
	dbinfo = fmt.Sprintf("host=%s port=5432 user=%s password=%s dbname=%s sslmode=disable", DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)

	db = createDBConnection()
)

type Assignment struct {
	ID             int       `db:"assignment_id"`
	Name           string    `db:"ass_name"`
	StartDate      DateTime  `db:"ass_start_date"`
	EndDate        DateTime  `db:"ass_end_date"`
	CreatedAt      Timestamp `db:"ass_created_at"`
	UpdatedAt      Timestamp `db:"ass_updated_at"`
	ProjectID      int       `db:"ass_project_id"`
	ResourceID     int       `db:"ass_resource_id"`
	Utilization    string    `db:"ass_utilization"`
	SemPredEndDate DateTime  `db:"ass_sem_predicted_end_date"`
	ExtPropability int       `db:"ass_sem_extension_probability"`
	Note           string    `db:"ass_note"`
	StatusID       int       `db:"ass_status_id"`
}

type AssignmentCsv struct {
	ID             int       `csv:"id"`
	StartDate      DateTime  `csv:"startdate"`
	EndDate        DateTime  `csv:"enddate"`
	ExtPropability int       `csv:"probability_of_salesperson_enddate"`
	SemPredEndDate DateTime  `csv:"sales_person_projected_enddate"`
	CreatedAt      Timestamp `csv:"created_at"`
	NotUsed0       string    `csv:"-"`
	ResourceName   string    `csv:"engineername"`
	ProjectName    string    `csv:"code"`
	ResourceMSU    string    `csv:"name"`
	Description    string    `csv:"description"`
}

type Id struct {
	ID int64 `db:"id"`
}

//Utilization    int       `csv:"ass_utilization"`

func main() {

	// Setup
	gocsv.SetCSVReader(func(in io.Reader) *csv.Reader {
		//return csv.NewReader(in)
		return CustomCSVReader(in) // Allows use of quotes in CSV
	})

	// Open csv file
	assignmentsCsvFile, err := os.OpenFile("import.csv", os.O_RDONLY, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer assignmentsCsvFile.Close()

	// Loads Assignments form csv
	assignmentsCsv := []*AssignmentCsv{}

	if err := gocsv.UnmarshalFile(assignmentsCsvFile, &assignmentsCsv); err != nil {
		panic(err)
	}

	// Prepate data
	assignments := make([]Assignment, 0)
	for _, item := range assignmentsCsv {

		// Find project ID
		var projectID int
		row := db.QueryRow(QUERY_PROJECT_CODE, "%"+item.ProjectName+"%")
		err = row.Scan(&projectID)
		if err == sql.ErrNoRows {
			continue
		}
		checkErr(err)

		var resourceID int
		row = db.QueryRow(QUERY_RESOURCE_ID, "%"+item.ResourceName+"%")
		err = row.Scan(&resourceID)
		if err == sql.ErrNoRows {
			continue
		}
		checkErr(err)

		fmt.Printf("%v\n", resourceID)

		tmp := Assignment{}
		tmp.Name = item.ProjectName + " Imported #" + strconv.Itoa(item.ID)
		tmp.CreatedAt = item.CreatedAt
		tmp.EndDate = item.EndDate
		tmp.StartDate = item.StartDate
		tmp.ExtPropability = item.ExtPropability
		tmp.ResourceID = resourceID
		tmp.ProjectID = projectID

		//tmp.UpdatedAt = time.Now().UTC()

		assignments = append(assignments, tmp)
		fmt.Printf("%v\n", tmp)
	}

	fmt.Println(assignments[12])

}

func CustomCSVReader(in io.Reader) *csv.Reader {
	csvReader := csv.NewReader(in)
	csvReader.LazyQuotes = true
	csvReader.TrimLeadingSpace = false
	csvReader.Comma = 'ý'
	return csvReader
}

type Timestamp struct {
	time.Time
}

func (date *Timestamp) MarshalCSV() (string, error) {
	return date.Time.Format("2006-01-02 15:04:05"), nil
}

func (date *Timestamp) String() string {
	return date.String() // Redundant, just for example
}

func (date *Timestamp) UnmarshalCSV(csv string) (err error) {
	date.Time, err = time.Parse("2006-01-02 15:04:05", csv)
	if err != nil {
		return err
	}
	return nil
}

type DateTime struct {
	time.Time
}

func (date *DateTime) MarshalCSV() (string, error) {
	return date.Time.Format("2006-01-02 15:04:05"), nil
}

func (date *DateTime) String() string {
	return date.String() // Redundant, just for example
}

func (date *DateTime) UnmarshalCSV(csv string) (err error) {
	if csv == "" {
		return nil
	}
	date.Time, err = time.Parse("2006-01-02", csv)
	if err != nil {
		return err
	}
	return nil
}

func createDBConnection() *sqlx.DB {
	db, err := sqlx.Connect("postgres", dbinfo)
	if err != nil {
		log.Fatalln(err)
	}
	checkErr(err)
	checkErr(db.Ping())
	db.SetMaxIdleConns(N_CONNS)
	return db
}

func checkErr(err error) {
	if err != nil {
		fmt.Printf("Unexpected behaviour occurred: %s \n", err)
		panic(err)
	}
}
